<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Service\Display;

use App\Service\Config\ConfigService;
use App\Service\Display\Data\DataFormatter;
use App\Service\Display\Data\StructuredData;
use App\Service\Display\Entity\Header;
use App\Service\Display\Section\SectionService;
use App\Service\Display\Widget\WidgetService;
use App\Service\Language\LanguageService;
use App\Service\Ontology\Builder\Entity\RDFResource;
use App\Service\Ontology\Context\ContextService;
use App\Service\Ontology\Hierarchy\HierarchyService;
use App\Service\Ontology\Seeker\SeekerService;
use App\Service\Ontology\Storer\StorerService;
use App\Service\Ontology\Builder\Entity\RDFGraph;
use App\Supplier\RDF\RDF;
use App\Supplier\Widget\Event;
use App\Supplier\Widget\Location;
use Twig_Environment;

/**
 * Class DisplayService
 * @package App\Service\Display
 */
class DisplayService
{
    const MAIN_LANGCODE = 'fr';

    /**
     * Section Service
     * @var SectionService
     */
    public $section;

    /**
     * Widget Service
     * @var WidgetService
     */
    public $widget;

    /**
     * Graph
     * @var RDFGraph
     */
    private $graph;

    /**
     * Structured data
     * @var StructuredData
     */
    private $structuredData;

    /**
     * Data Formatter
     * @var DataFormatter
     */
    private $dataFormatter;

    /**
     * Config Service
     * @var ConfigService
     */
    private $config;

    /**
     * DisplayService constructor.
     * @param ContextService $context
     * @param HierarchyService $hierarchy
     * @param StorerService $storer
     * @param SeekerService $seeker
     * @param LanguageService $lang
     * @param Twig_Environment $twig
     */
    public function __construct(ContextService $context, HierarchyService $hierarchy, StorerService $storer,
                                SeekerService $seeker, LanguageService $lang, Twig_Environment $twig,
                                ConfigService $config
    ) {
        $this->initServices($context, $hierarchy, $storer, $seeker, $lang, $twig, $config);
    }

    /**
     * Returns structured data ready for template display.
     * @param RDFGraph $graph
     * @return StructuredData
     */
    public function getStructuredData(RDFGraph $graph): StructuredData
    {
        if (!$this->structuredData)
            $this->initStructuredData($graph);

        return $this->structuredData;
    }

    /**
     * Manually sets the object of structured data, overriding any existing one.
     * This method is public for flexibility purpose but should not be used
     * out of the box in a normal usage of the service.
     * @param StructuredData $structuredData
     * @return DisplayService
     */
    public function setStructuredData(StructuredData $structuredData): self
    {
        $this->structuredData = $structuredData;
        return $this;
    }

    /**
     * Resets structured data.
     * @return self
     */
    public function clearStructuredData(): self
    {
        if ($this->structuredData)
            unset($this->structuredData);

        return $this;
    }

    /**
     * Sets RDFGraph.
     * @param RDFGraph $graph
     * @return self
     */
    public function setGraph(RDFGraph $graph): self
    {
        $this->graph = $graph;
        return $this;
    }

    /**
     * Gets RDFGraph.
     * @return RDFGraph|null
     */
    public function getGraph()
    {
        return $this->graph;
    }

    /**
     * Initializes structured data.
     * @param RDFGraph $graph
     * @return self
     */
    private function initStructuredData(RDFGraph $graph): self
    {
        $structuredData = $this->createStructuredData($graph);
        $this
            ->populateUri($structuredData)
            ->populateTitle($structuredData)
            ->populateDescription($structuredData)
            ->populateTypes($structuredData)
            ->populateSections($structuredData)
            ->populateUndescribed($structuredData)
            ->populateWidgets($structuredData)
        ;

        $this->setStructuredData($structuredData);

        return $this;
    }

    /**
     * @param RDFGraph $graph
     * @return StructuredData
     */
    private function createStructuredData(RDFGraph $graph): StructuredData
    {
        return new StructuredData($graph);
    }

    /**
     * Populates URI.
     * @param StructuredData $structuredData
     * @return DisplayService
     */
    private function populateUri(StructuredData &$structuredData): self
    {
        $structuredUri = array();

        $uri = $structuredData->getGraph()->getUri();
        $structuredUri['long']  = $uri;
        $structuredUri['short'] = $this->section->context->shorten($uri);

        $structuredData->setUri($structuredUri);

        return $this;
    }

    /**
     * Populates label.
     * @param StructuredData $structuredData
     * @return DisplayService
     */
    private function populateTitle(StructuredData &$structuredData): self
    {
        $structuredTitle = array();

        if ($resource = $structuredData->getGraph()->getMasterResource()->getResource(RDF::MAIN_LABEL_URI))
        {
            $label = $resource->getLeaf(self::MAIN_LANGCODE) ?: $resource->getLeaf();

            $structuredTitle['label'] = $label;
            $structuredTitle['uri']   = $structuredData->getGraph()->getUri() ?? null;
        }

        $structuredData->setTitle($structuredTitle);

        return $this;
    }

    /**
     * Populates description.
     * @param StructuredData $structuredData
     * @return DisplayService
     */
    private function populateDescription(StructuredData &$structuredData): self
    {
        $structuredDescription = null;

        if ($resource = $structuredData->getGraph()->getMasterResource()->getResource(RDF::MAIN_COMMENT_URI))
        {
            $description = $resource->getLeaf(self::MAIN_LANGCODE) ?: $resource->getLeaf();

            $structuredDescription = $description;
        }

        $structuredData->setDescription($structuredDescription);

        return $this;
    }

    /**
     * Populates types.
     * @param StructuredData $structuredData
     * @return DisplayService
     */
    private function populateTypes(StructuredData &$structuredData): self
    {
        $structuredTypes = array();

        if ($resource = $structuredData->getGraph()->getMasterResource()->getResource(RDF::MAIN_TYPE_URI))
        {
            $typeResources = $resource->getResources();
            $types = array_merge(
                $typeResources !== null ? array_keys($typeResources->getArrayCopy()) : [],
                $resource->getLeaves() ?? []
            );
            for ($i = 0, $count = count($types); $i < $count; $i++)
            {
                $structuredTypes[$i]['label'] = $this->section->seeker->findClassLabel($types[$i]);
                $structuredTypes[$i]['uri']   = $this->section->context->expand($types[$i]);
            }
        }

        $structuredData->setTypes($structuredTypes);

        return $this;
    }

    /**
     * Populates sections.
     * @param StructuredData $structuredData
     * @return DisplayService
     */
    private function populateSections(StructuredData &$structuredData): self
    {
        $structuredSections = array();
        $structuredSections = $this->section->getAvailableSections($structuredData->getGraph());
        $structuredSections = $this->section->prioritizeSections($structuredSections);
        $structuredData->setSections($structuredSections);

        return $this;
    }

    /**
     * @param StructuredData $structuredData
     * @return DisplayService
     */
    private function populateUndescribed(StructuredData &$structuredData): self
    {
        $irrelevantMetaUris = array(
            RDF::MAIN_ID_URI,
            RDF::MAIN_TYPE_URI,
            RDF::MAIN_LABEL_URI
        );
        $irrelevantMetaNs = $this->config->getParameter('hidden_namespaces');

        // Getting all undescribed resources
        $undescribedResources = (function () use (&$structuredData): array {
            $undescribedResources = array();
            $resources = $structuredData->getGraph()->getMasterResource()->getResources();
            foreach (array_keys($resources->getArrayCopy()) as $resourceUri)
            {
                $found = false;
                foreach ($structuredData->getSections() as $section)
                    foreach ($section->getProperties() as $property)
                        if ($property->getUri() === $resourceUri)
                            $found = true;
                if ($found === false)
                    $undescribedResources[] = $resources[$resourceUri];
            }
            return $undescribedResources;
        })();

        // Filtering irrelevant URIs and namespaces
        $unsetMeta = function (array $metaPropertiesUris, array $metaPropertiesNs, array $resources) {
            $unsetMeta = array();
            foreach ($resources as $resource) {
                $uri = $resource->getUri();
                $available = function () use (&$metaPropertiesUris, &$metaPropertiesNs, &$uri): bool {
                    if (in_array($uri, $metaPropertiesUris, true))
                        return false;
                    foreach ($metaPropertiesNs as $ns)
                        if (strpos($uri, $ns) === 0)
                            return false;
                    return true;
                };
                if ($available() === true)
                    $unsetMeta[] = $resource;
            }
            return $unsetMeta;
        };

        $undescribedResources = $unsetMeta($irrelevantMetaUris, $irrelevantMetaNs, $undescribedResources);
        $structuredData->setUndescribed($undescribedResources);

        return $this;
    }

    /**
     * Populates widgets.
     * @param StructuredData $structuredData
     * @return self
     */
    private function populateWidgets(StructuredData &$structuredData): self
    {
        $structuredWidgets = array();
        $structuredWidgets  = $this->widget->getAvailableWidgets($structuredData->getGraph());
        $structuredData->setWidgets($structuredWidgets);

        return $this;
    }

    /**
     * Initializes internal services.
     * @param ContextService $context
     * @param HierarchyService $hierarchy
     * @param StorerService $storer
     * @param SeekerService $seeker
     * @param LanguageService $lang
     * @param Twig_Environment $twig
     * @return self
     */
    private function initServices(ContextService $context, HierarchyService $hierarchy, StorerService $storer,
                                  SeekerService $seeker, LanguageService $lang, Twig_Environment $twig,
                                  ConfigService $config
    ): self {
        $this->dataFormatter = new DataFormatter(
            $context, $lang
        );

        $this->config = $config;

        $this->section = (function() use (&$context, &$hierarchy, &$storer, &$seeker, &$lang) {
            return new SectionService(
                $context,
                $hierarchy,
                $storer,
                $seeker,
                $this->dataFormatter
            );
        })();

        $this->widget  = (function() use ($twig) {
            return new WidgetService(
                $twig,
                array(
                    new Event(),
                    new Location()
                )
            );
        })();

        return $this;
    }
}
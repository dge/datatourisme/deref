<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Error\Exception\Format\RDF;

use App\Error\Exception\Format\InvalidFormatException;

/**
 * Class InvalidJsonLdException
 * @package App\Error\Exception\Format\RDF
 */
class InvalidJsonLdException extends InvalidFormatException
{
}
<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Service\Ontology\Builder\Entity;

use App\Error\Exception\Value\RDF\InvalidLiteralException;
use App\Service\Display\Data\DataFormatter;
use App\Service\Ontology\Builder\Collection\RDFResourceCollection;
use App\Supplier\RDF\RDF;

/**
 * RDF Resource
 * @package App\Service\Ontology\Builder\Entity
 */
class RDFResource
{
    /**
     * URI of the RDFResource.
     * @var string
     */
    private $uri;

    /**
     * Collection of sub-RDFResource owned by current RDFResource.
     * @var RDFResourceCollection
     */
    private $resources;

    /**
     * Array of resource's local "leaves", i.e. scalar literals.
     * @var array
     */
    private $leaves;

    /**
     * RDFSource
     * @var RDFSource
     */
    private $source;

    /**
     * RDFResource constructor.
     * @param string $uri
     */
    public function __construct(string $uri)
    {
        $this->uri = $uri;
        $this->resources = new RDFResourceCollection();
        $this->leaves = [];
    }

    /**
     * Recursively builds RDFResource.
     * @param array $triples
     * @return self
     */
    public function build(array $triples): self
    {
        foreach ($triples as $key => $value)
        {
            if (is_string($key))
            {
                $resource = new RDFResource($key);
                $resource = $resource->build($value);
                $this->addResource($resource);
            }
            else
            {
                $this->addLeaf($value);
            }

        }

        return $this;
    }

    /**
     * Gets resources's label.
     * @param string|null $lang
     * @return mixed|null
     */
    public function getLabel(string $lang = null, bool $strict = false)
    {
        $uri = RDF::MAIN_LABEL_URI;
        return $this->getLiteral($uri, $lang, $strict);
    }

    /**
     * Sets resource's label.
     * @param string $label
     * @return self
     */
    public function setLabel(string $label): self
    {
        $labelResource = new RDFResource(RDF::MAIN_LABEL_URI);
        $labelResource->addLeaf($label);
        $this->addResource($labelResource);

        return $this;
    }

    /**
     * Gets resource's comment.
     * @param string|null $lang
     * @param bool $strict
     * @return mixed|null
     */
    public function getComment(string $lang = null, bool $strict = false)
    {
        $uri = RDF::MAIN_COMMENT_URI;
        return $this->getLiteral($uri, $lang, $strict);
    }

    /**
     * Sets resource's comment.
     * @param string $comment
     * @return self
     */
    public function setComment(string $comment): self
    {
        $commentResource = new RDFResource(RDF::MAIN_COMMENT_URI);
        $commentResource->addLeaf($comment);
        $this->addResource($commentResource);

        return $this;
    }

    /**
     * Gets literal by its URI.
     *
     * @param string $uri
     * @param string|null   $lang    Lang tag for literal. Examples: `@fr` or `en-US`
     * @param bool          $strict  If strict is true, null will be returned if lang is not found
     *                               Else if strict is false, first literal or null will be returned
     * @return string|null
     */
    public function getLiteral(string $uri, string $lang = null, bool $strict = false)
    {
        $resource = $this->getResource($uri);
        if (!$resource)
            return null;

        $literals = $resource->getLeaves();

        if ($lang === null)
            return array_shift($literals);

        foreach ($literals as $literal)
        {
            $lowLang = strtolower($lang);
            if (preg_match('~@([a-zA-Z]{2}(?:-[a-zA-Z]{2})?)$~', $literal, $matches) === 1)
                if (strtolower($matches[0]) === $lowLang || strtolower($matches[1]) === $lowLang)
                    return $literal;
        }

        return $strict === true ? null : array_shift($literals);
    }

    /**
     * Gets resource's URI.
     * @return string
     */
    public function getUri(): string
    {
        return $this->uri;
    }

    /**
     * Gets all local resources' uris of the resource.
     * @return array|null
     */
    public function getUris()
    {
        if ($this->resources->count() === 0)
            return null;

        $uris = [];
        foreach ($this->resources as $resource)
            $uris[] = $resource->geturi();

        return $uris;
    }

    /**
     * Sets resource's URI.
     * @param string $uri
     * @return self
     */
    public function setUri(string $uri): self
    {
        $this->uri = $uri;

        return $this;
    }

    /**
     * Gets resource's resources.
     * @return RDFResourceCollection
     */
    public function getResources(): RDFResourceCollection
    {
        return $this->resources;
    }

    /**
     * Gets resource's first resource
     * @return RDFResource|null
     */
    public function getFirstResource()
    {
        if ($this->resources->count() === 0)
            return null;

        $copy = $this->resources->getArrayCopy();
        return array_shift($copy);
    }

    /**
     * Gets one resource from current resource, by its URI.
     * @param string $uri
     * @return mixed|null
     * @throws \Exception
     */
    public function getResource(string $uri)
    {
        if (!isset($this->resources[$uri]))
            return null;

        if (!$this->resources[$uri] instanceof RDFResource)
            throw new \UnexpectedValueException("Requested resource is not an instance of ".RDFResource::class);

        return $this->resources[$uri];
    }

    /**
     * Sets resource's resources. (Overrides existing ones.)
     * @param RDFResourceCollection|RDFResource[] $resources
     * @return self
     * @throws \UnexpectedValueException
     */
    public function setResources(array $resources): self
    {
        if ($resources instanceof RDFResourceCollection)
            $this->resources = $resources;
        else if (is_iterable($resources))
            $this->resources = new RDFResourceCollection($resources);
        else
            throw new \UnexpectedValueException(
                "Can only take array or ".RDFResourceCollection::class." as argument.");

        return $this;
    }

    /**
     * Adds an array of resources to the resource's resources.
     * @param RDFResource[] $resources
     * @return self
     */
    public function addResources(array $resources): self
    {
        foreach ($resources as $resource)
            $this->resources->add($resource);

        return $this;
    }

    /**
     * Adds one resource to resource's resources.
     * @param RDFResource $resource
     * @return self
     */
    public function addResource(RDFResource $resource): self
    {
        $this->resources->add($resource);

        return $this;
    }

    /**
     * Gets all resource's local leaves.
     * @return array
     */
    public function getLeaves(): array
    {
        return $this->leaves;
    }

    /**
     * Returns resource's first leaf.
     * @return mixed|null
     */
    public function getFirstLeaf()
    {
        return $this->getLeaf(0);
    }

    /**
     * Returns resource's local unique leaf by its numeric key or langcode.
     *
     * If no key and no langcode are provided, the first value is returned.
     *
     * @param int|string|null $key
     * @return mixed|null
     */
    public function getLeaf($key = null)
    {
        switch (strtolower(gettype($key))) {
            case 'null':
                return $this->leaves[0];
            case 'integer':
                return $this->leaves[$key];
            case 'string':
                return $this->getLeafByLangcode($key);
            default:
                return null;
        }
    }

    /**
     * Sets resource's local leaves. (Overrides existing ones.)
     * @param array $leaves
     * @return self
     * @throws \Exception
     */
    public function setLeaves(array $leaves): self
    {
        foreach ($leaves as $leaf)
            if (!is_scalar($leaf))
                throw new InvalidLiteralException(
                    "\"$leaf\" is not a scalar value and cannot be considered as an RDF literal.");

        $this->leaves = $leaves;

        return $this;
    }

    /**
     * Adds an array of leaves to the resource's local leaves.
     * @param array $leaves
     * @return self
     * @throws \Exception
     */
    public function addLeaves(array $leaves): self
    {
        foreach ($leaves as $leaf)
            if (!is_scalar($leaf))
                throw new InvalidLiteralException(
                    "\"$leaf\" is not a scalar and cannot be considered as a RDF literal.");

        $this->leaves = array_merge($this->leaves, $leaves);

        return $this;
    }

    /**
     * Adds one leaf to resource's local leaves.
     * @param string $leaf
     * @return self
     * @throws \Exception
     */
    public function addLeaf(string $leaf): self
    {
        if (strlen($leaf) === 0)
            return $this;

        if (!is_scalar($leaf))
            throw new InvalidLiteralException(
                "\"$leaf\" is not a scalar and cannot be considered as a RDF literal.");

        $this->leaves[] = $leaf;

        return $this;
    }

    /**
     * Code is self-explanatory: returns a merged array of leaves and resources.
     * @return array
     */
    public function getAllValues(): array
    {
        $resources = $this->getResources();
        return array_merge(
            $resources !== null ? $resources->getArrayCopy() : [],
            $this->getLeaves() ?? []
        );
    }

    /**
     * Trys to get literal string leaf matching given langcode.
     *
     * Example:
     *   `getLeafByLangcode('en-gb')` could return `"Hello planet!"@en-gb`
     *   `getLeafByLangcode('ja') could return `Toyota!`@ja
     *
     * @param string $langcode
     *
     * @return string|null
     */
    private function getLeafByLangcode(string $langcode)
    {
        foreach ($this->leaves as $leaf)
            if (preg_match("~@{$langcode}$~", $leaf) === 1)
                return $leaf;

        return null;
    }

//    /**
//     * TODO
//     * Gets RDFResource's RDFSource.
//     * @return RDFSource|null
//     */
//    public function getSource()
//    {
//        if (!isset($this->source))
//            return null;
//
//        return $this->source;
//    }
//
//    /**
//     * TODO
//     * Sets RDFSource to the RDFResource.
//     * @param RDFSource $source
//     * @return self
//     */
//    public function setSource(RDFSource $source): self
//    {
//        $this->source = $source;
//        return $this;
//    }
}
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var gulp = require('gulp');
var glob = require('glob');
var path = require('path');
var livereload = require('gulp-livereload');
var es = require('event-stream');

var $ = require('gulp-load-plugins')({
    pattern: ['gulp-*']
});

/**
 * Variables
 */
var entries = glob.sync('./app/Resources/styles/*.scss');
var dstDir = './web/assets/styles';
var watchGlob = [
    './app/Resources/styles/**/*.scss',
    './common/datatourisme/webapp-bundle/Resources/styles/**/*.scss'
];

var sassParams = {
    errLogToConsole: true,
    sourceMap: 'sass',
    sourceComments: 'map',
    precision: 10,
    style: 'expanded'
};

/**
 * @param obfuscate
 * @param watch
 * @param dest
 */
function run(minify, dest) {
    var stream = es.merge(entries.map(function(entry) {
        return build(entry, minify, dest);
    }));
    return stream;
}

/**
 * @param minify
 * @param dest
 * @returns {*}
 */
function build(entry, minify, dest) {
    var startMs = Date.now();
    var b = gulp.src(entry)
        .pipe($.plumber(function(error) {
            $.util.log($.util.colors.red(error.message));
            $.util.log($.util.colors.red(error.stack));
            this.emit('end');
        }))
        .pipe($.concat(path.basename(entry)))
        .pipe($.sass(sassParams))
        .pipe($.autoprefixer(/*'last 1 version, ie 10, ie 11'*/));

    if(minify) {
        b = b.pipe($.cssmin());
    }

    console.log('update sass -', (Date.now() - startMs) + 'ms');
    b.pipe(gulp.dest(dest));
    return b;
}

/**
 * @type {{build: module.exports.build, watch: module.exports.watch}}
 */
module.exports = {
    build: function() {
        return run(true, dstDir);
    },
    watch: function() {
        livereload.listen();
        run(false, dstDir);
        $.watch(watchGlob, { ignoreInitial: true }, function() {
            run(false, dstDir).pipe(livereload());
        });
    }
};

<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Service\Ontology\Builder\Entity;
use App\Service\Ontology\Builder\Collection\RDFResourceCollection;

/**
 * Class RDFGraph
 * @package App\Service\Ontology\Builder\Entity
 */
class RDFGraph
{
    /**
     * Graph URI.
     * @var string|null
     */
    private $uri;

    /**
     * Graph's resources (instances of RDFResource).
     * @var RDFResourceCollection
     */
    private $resources;

    /**
     * Raw N-Triples source of the Graph
     * @var string
     */
    private $source;

    /**
     * RDFGraph constructor.
     * @param string|null $uri
     */
    public function __construct(string $uri = null, string $source = null)
    {
        $this->uri = $uri;
        $this->resources = new RDFResourceCollection();
        $this->source = $source;
    }

    /**
     * Recursively builds graph.
     * @param array $triples
     * @return self
     */
    public function build(array $triples): self
    {
        foreach ($triples as $key => $value)
        {
            $resource = new RDFResource($key);
            $resource->build($value);
            $this->addResource($resource);
        }

        return $this;
    }

    /**
     * Gets GRAPH Uri.
     * @return null|string
     */
    public function getUri()
    {
        return $this->uri ?? null;
    }

    /**
     * Gets all local resources' uris of the graph.
     * @return array
     */
    public function getUris(): array
    {
        if ($this->resources->count() === 0)
            return [];

        $uris = array();
        foreach ($this->resources as $resource)
            $uris[] = $resource->getUri();

        return $uris;
    }

    /**
     * Sets graph URI.
     * @param string $uri
     * @return self
     */
    public function setUri(string $uri): self
    {
        $this->uri = $uri;

        return $this;
    }

    /**
     * Gets graph's resources.
     * @return RDFResourceCollection
     * @throws \Exception
     */
    public function getResources()
    {
        return $this->resources;
    }

    /**
     * If Graph's URI is set, returns the Resource matching to the URI.
     * Else returns null
     *
     * @return RDFResource|null
     */
    public function getMasterResource()
    {
        if ($this->getUri() !== null)
            return $this->getResource($this->getUri());

        return null;
    }

    /**
     * Gets graphs's first resource
     * @return RDFResource|null
     */
    public function getFirstResource()
    {
        if ($this->resources->count() === 0)
            return null;

        return array_shift($this->resources->getArrayCopy());
    }

    /**
     * Gets one resource by its URI.
     * @param string $path
     * @return mixed|null
     * @throws \Exception
     */
    public function getResource(string $path)
    {
        if (!isset($this->resources[$path]))
            return null;

        if (!$this->resources[$path] instanceof RDFResource)
            throw new \UnexpectedValueException("Requested resource is not an instance of ".RDFResource::class);

        return $this->resources[$path];
    }

    /**
     * Sets graph's resources. (Overrides existing ones.)
     * @param array|RDFResourceCollection $resources
     * @return self
     * @throws \UnexpectedValueException
     */
    public function setResources($resources): self
    {
        if ($resources instanceof RDFResourceCollection)
            $this->resources = $resources;
        else if (is_array($resources))
            $this->resources = new RDFResourceCollection($resources);
        else
            throw new \UnexpectedValueException(
                "Can only take array or ".RDFResourceCollection::class." as argument.");

        return $this;
    }

    /**
     * Adds an array of resources to the graph's resources.
     * @param RDFResource[] $resources
     * @return self
     * @throws \Exception
     */
    public function addResources(array $resources): self
    {
        foreach ($resources as $resource)
            $this->resources->add($resource);

        return $this;
    }

    /**
     * Adds one resource to graph's resources.
     * @param RDFResource $resource
     * @return self
     */
    public function addResource(RDFResource $resource): self
    {
        $this->resources->add($resource);

        return $this;
    }

    /**
     * Returns N-Triples RDF source of the Graph
     * @return string|null
     */
    public function getSource()
    {
        if (!is_string($this->source) && $this->source !== null)
            throw new \UnexpectedValueException(
                "Source should be string or null, ".gettype($this->source)." found.");

        return $this->source;
    }

    /**
     * Sets N-Triples RDF source of the Graph
     * @param string $source
     * @return RDFGraph
     */
    public function setSource(string $source): self
    {
        $this->source = $source;
        return $this;
    }
}
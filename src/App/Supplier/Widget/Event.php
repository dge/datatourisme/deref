<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Supplier\Widget;

use App\Service\Display\Widget\AbstractWidget;
use App\Service\Display\Widget\AbstractWidgetData;
use App\Sparql\SparqlClient;
use Symfony\Component\Yaml\Yaml;

/**
 * Class Event
 * @package App\Widget
 */
class Event extends AbstractWidget
{
    /**
     * {@inheritdoc}
     */
    public function __construct(string $templateFile = "app/widgets/event.twig", bool $uniqueProperties = false)
    {
        parent::__construct($templateFile, $uniqueProperties);
    }

    /**
     * {@inheritdoc}
     */
    public function isAvailable(): bool
    {
        if (!$this->graph)
            throw new \Exception(
                "Widget must be populated with dereferenced RDFGraph" .
                "before being candidate for display availability.");

        return null !== $this->graph->getMasterResource()->getResource(
                "https://www.datatourisme.gouv.fr/ontology/core#takesPlaceAt"
            );
    }

    /**
     * {@inheritdoc}
     */
    public function getData()
    {
        if ($this->data instanceof AbstractWidgetData)
            return $this->data;

        $config = Yaml::parse(file_get_contents('../app/config/parameters.yml'));
        $sparqlEndpoint = $config['parameters']['suppliers']['widgets']['event']['endpoint'] ?? null;
        $sparqlFallbackEndpoints = $config['parameters']['suppliers']['widgets']['event']['fallback_endpoints'] ?? null;

        if (!$sparqlEndpoint)
            return null;

        $uri = $this->graph->getUri();

        $selects = array(
            'startDate'  => '?startDate',
            'endDate'    => '?endDate',
        );

        $query = "
            prefix : <https://www.datatourisme.gouv.fr/ontology/core/1.0#>
            prefix schema: <http://schema.org/>

            SELECT ".implode(" ", $selects)."
            WHERE
            {
              <$uri>        :takesPlaceAt     ?eventNode   .
              ?eventNode    :startDate        ?startDate   .
              ?eventNode    :endDate          ?endDate     .
            }
        ";

        $client = new SparqlClient($sparqlEndpoint, $sparqlFallbackEndpoints);
        $client->setHeaders(["Accept: application/json"]);
        $result = $client->query($query);
        $result = json_decode($result, true);

        if (!isset($result['results']['bindings']) || empty($result['results']['bindings']))
            return null;

        $data = array();
        $bindings = &$result['results']['bindings'];  // Just for variable readability
        for ($i = 0, $count = count($bindings); $i < $count; ++$i)
            foreach ($bindings[$i] as $var => $values)
                $data[array_search("?$var", $selects)] = $values['value'] ?? null;

        if (!isset($data['startDate']) || !isset($data['endDate']))
            return null;

        $startDate = new \DateTime($data['startDate']);
        $endDate = new \DateTime($data['endDate']);
        $eventData = new EventData($startDate, $endDate);

        $this->setData($eventData);

        return $this->getData();
    }
}
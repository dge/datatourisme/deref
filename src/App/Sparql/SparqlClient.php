<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Sparql;

use App\Error\Exception\Query\QueryFailedException;
use App\Error\Exception\Query\Sparql\SparqlQueryFailedException;

/**
 * Class SparqlClient
 * @package App\Service\Sparql
 */
class SparqlClient
{
    /**
     * @var string
     */
    private $endpoint;

    /**
     * @var string[]
     */
    private $fallbackEndpoints;

    /**
     * @var array
     */
    private $headers = array();

    /**
     * @var bool
     */
    private $useFallbacks = true;

    /**
     * @var bool
     */
    private $hasFallbacked = false;

    /**
     * SparqlClient constructor.
     * @param string $endpoint
     * @param array $fallbackEndpoints
     */
    public function __construct(string $endpoint, array $fallbackEndpoints = array())
    {
        $this->setEndpoint($endpoint);
        $this->setFallbackEndpoints($fallbackEndpoints);
    }

    /**
     * @param string $query
     * @param string|null $endpoint
     * @return string
     * @throws SparqlQueryFailedException
     */
    public function query(string $query, string $endpoint = null): string
    {
        $ch = curl_init();

        $endpoint = $endpoint ?: $this->endpoint;

        curl_setopt_array($ch, array(
            CURLOPT_URL => $endpoint.'?query='.rawurlencode($query),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => $this->headers
        ));

        $result = curl_exec($ch);

        if ($result === false)
            throw new SparqlQueryFailedException('Query failed. Message: \"'.curl_error($ch).'\"');

        curl_close($ch);

        $result = preg_replace('/\\\\u([\da-fA-F]{4})/', '&#x\1;', $result);
        $result = html_entity_decode($result);

        if (empty($result) && $this->doUseFallbacks()) {
            foreach ($this->fallbackEndpoints as $fallbackEndpoint) {
                $result = $this->fallbackQuery($query, $fallbackEndpoint);
                if (!empty($result)) {
                    return $result;
                }
            }
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getEndpoint(): string
    {
        return $this->endpoint;
    }

    /**
     * @param string $endpoint
     * @return self
     */
    public function setEndpoint(string $endpoint): self
    {
        $this->endpoint = $endpoint;
        return $this;
    }

    /**
     * @return string
     */
    public function getFallbackEndpoint(): string
    {
        return $this->fallbackEndpoint;
    }

    /**
     * @param string[] $fallbackEndpoints
     * @return self
     */
    public function setFallbackEndpoints(array $fallbackEndpoints): self
    {
        foreach ($fallbackEndpoints as $val)
            if (!is_string($val))
                throw new \UnexpectedValueException("String expected for endpoint url, " . gettype($val) . " given.");

        $this->fallbackEndpoints = $fallbackEndpoints;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param array $headers
     * @return self
     */
    public function setHeaders(array $headers): self
    {
        $this->headers = $headers;
        return $this;
    }

    /**
     * @return bool
     */
    public function doUseFallbacks(): bool
    {
        return $this->useFallbacks;
    }

    /**
     * @return self
     */
    public function disableFallbacks(): self
    {
        $this->useFallbacks = false;
        return $this;
    }

    /**
     * @return self
     */
    public function enableFallbacks(): self
    {
        $this->useFallbacks = true;
        return $this;
    }

    public function hasFallbacked(): bool
    {
        return $this->hasFallbacked;
    }

    /**
     * @param string $query
     * @param string $endpoint
     * @return string
     * @throws SparqlQueryFailedException
     */
    private function fallbackQuery(string $query, string $endpoint): string
    {
        $client = new self($endpoint);
        $client->setHeaders($this->headers);
        $result = $client->query($query, $endpoint);
        $this->hasFallbacked = true;

        return $result;
    }
}
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
"use strict";

let References = require('./References.js');

/**
 * References app.
 */
document.addEventListener("DOMContentLoaded", function()
{
    let reference = new References();
    let container = document.getElementById("references");

    try {
        reference.run(container);
    } catch (e) {
        console.log(e);
    }
});

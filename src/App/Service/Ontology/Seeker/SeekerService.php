<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Service\Ontology\Seeker;

use App\Service\Ontology\Context\ContextService;
use App\Service\Ontology\Storer\StorerService;
use Exception;

/**
 * Service designed to explore stored ontologies and get data.
 * @package App\Service\Seeker
 */
class SeekerService
{
    /**
     * Storer Service
     * @var StorerService
     */
    public $storer;

    /**
     * Context Service
     * @var ContextService
     */
    public $context;

    /**
     * Array of ontologies.
     * @var array
     */
    private $ontologies;

    /**
     * Index identifiers.
     */
    private $indexes;

    /**
     * SeekerService constructor.
     * @param StorerService $storer
     * @param ContextService $context
     * @param array $indexes
     * @throws Exception
     */
    public function __construct(StorerService $storer, ContextService $context, array $indexes = array())
    {
        foreach ($indexes as $index)
            if (!is_string($index))
                throw new Exception("Index identifiers must be strings.");

        $defaultIndexes = array(
            'label'      => 'label',
            'comment'    => 'comment',
            'properties' => 'properties',
            'parents'    => 'superClasses',
            'ranges'     => 'ranges',
            'priority'   => 'priority'
        );

        $this->indexes = array_merge($defaultIndexes, $indexes);

        $this->storer = $storer;
        $this->context = $context;

        $this->ontologies = $this->storer->getIndexedOntologies();
    }

    /**
     * Recursively walks through iterable to apply callbacks on nodes.
     *
     * Useful when you do not control data and cannot know if nodes are going to be scalars or iterables.
     *
     * If strict mode is set to true, throws an exception when current node is neither iterable nor scalar. Else, just returns false and continues recursivity.
     *
     * @param $node
     * @param callable|null $scalarCallback Applied to iterables' leaves (scalar values).
     * @param callable|null $iterableCallback Applied to iterable nodes.
     * @param bool $strict
     * @return bool
     */
    static public function recursiveNodeWalk(&$node, callable $scalarCallback = null, callable $iterableCallback = null, bool $strict = false): bool
    {
        if (is_scalar($node))
        {
            if (is_callable($scalarCallback))
                $scalarCallback($node);
        }
        else if (is_iterable($node))
        {
            if (is_callable($iterableCallback))
                $iterableCallback($node);
            foreach ($node as $subNode)
                self::recursiveNodeWalk($subNode, $scalarCallback, $iterableCallback);
        }
        else
        {
            switch ($strict)
            {
                case false:
                    return false;
                case true:
                    $errorValue = gettype($node) !== 'object' ? gettype($node) : get_class($node);
                    throw new \UnexpectedValueException("Given node must be array or scalar, $errorValue given.");
                default:
                    throw new \LogicException("This code should not be reached. Probable cause: meteorites.");
            }
        }

        return true;
    }

    /**
     * Returns ontologies associated to a given URI, or returns empty array if it does not find any.
     * @param string $uri
     * @return array
     */
    public function findOntologies(string $uri)
    {
        $ontology = array();

        foreach ($this->ontologies as $url => $ontology)
            if (strpos($url, parse_url($uri, PHP_URL_HOST)) !== false)
                $ontology[$url] = $ontology;

        return $ontology;
    }

    /**
     * Gets class priority.
     * @param string $class
     * @return float|null
     */
    public function findClassPriority(string $class)
    {
        $priority = $this->findIndexedValue($class, $this->indexes['priority']);

        if (!is_string($priority) && !is_float($priority) && !is_int($priority) && !is_null($priority))
            throw new \UnexpectedValueException(
                "Can only process string, float, integer or null, ".gettype($priority)." found.");

        return (float) $priority;
    }

    /**
     * Gets class label.
     * @param string $class
     * @return string|null
     * @throws \UnexpectedValueException()
     */
    public function findClassLabel(string $class)
    {
        $label = $this->findIndexedValue($class, $this->indexes['label']);

        if (!is_string($label) && !is_null($label))
            throw new \UnexpectedValueException("findClassLabel method can only return string or null.");

        return $label;
    }

    /**
     * Gets class comment.
     * @param string $class
     * @return string|null
     * @throws \UnexpectedValueException()
     */
    public function findClassComment(string $class)
    {
        $comment = $this->findIndexedValue($class, $this->indexes['comment']);

        if (!is_string($comment) && !is_null($comment))
            throw new \UnexpectedValueException("findClassComment method can only return string or null.");

        return $comment;
    }

    /**
     * Gets class parents.
     * @param string $class
     * @return array|null
     * @throws \UnexpectedValueException()
     */
    public function findClassParents(string $class)
    {
        $parents = $this->findIndexedValue($class, $this->indexes['parents']);

        if (!is_array($parents) && !is_null($parents))
            throw new \UnexpectedValueException("findClassParents method can only return array or null.");

        return $parents;
    }

    /**
     * Gets class properties.
     * @param string $class
     * @return array|null
     * @throws \UnexpectedValueException()
     */
    public function findClassProperties(string $class)
    {
        $properties = $this->findIndexedValue($class, $this->indexes['properties']);

        if (!is_array($properties) && !is_null($properties))
            throw new \UnexpectedValueException("findClassProperties method can only return array or null.");

        return $properties;
    }

    /**
     * Gets class ranges.
     * @param string $class
     * @return array|null
     * @throws \UnexpectedValueException()
     */
    public function findClassRanges(string $class)
    {
        $ranges = $this->findIndexedValue($class, $this->indexes['ranges']);

        if (!is_array($ranges) && !is_null($ranges))
            throw new \UnexpectedValueException("findClassRanges method can only return array or null.");

        return $ranges;
    }

    /**
     * Returns value of a class according to given index.
     *
     * / ! \ The return type is not internally tested. Must be used carefully.
     * @param string $class
     * @param string $index
     * @return mixed
     */
    private function findIndexedValue(string $class, string $index)
    {
        foreach ($this->ontologies as $url => $ontology)
        {
            $guess = function (string $key) use (&$ontology, &$class)
            {
                return $ontology[$key][$class]
                    ?? $ontology[$key][$this->context->shorten($class)]
                    ?? $ontology[$key][$this->context->expand($class)]
                    ?? null;
            };

            $class = $guess('classes')
                ?? $guess('properties')
                ?? $guess('individuals')
                ?? null;

            return $class[$index] ?? null;
        }

        return null;
    }
}
<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Service\Ontology\Hierarchy;
use App\Service\Ontology\Context\ContextService;
use App\Service\Ontology\Seeker\SeekerService;
use App\Service\Ontology\Storer\StorerService;
use Exception;

/**
 * Service designed to build properties hierarchies upon stored ontologies.
 */
class HierarchyService
{
    /**
     * Array of ontologies.
     * @var array
     */
    private $ontologies;

    /**
     * Storer Service
     * @var StorerService
     */
    public $storer;

    /**
     * Seeker Service
     * @var SeekerService
     */
    public $seeker;

    /**
     * Context Service
     * @var ContextService
     */
    public $context;

    /**
     * HierarchyService constructor.
     * @param StorerService $storer
     * @param SeekerService $seeker
     * @param ContextService $context
     */
    public function __construct(StorerService $storer, SeekerService $seeker, ContextService $context)
    {
        $this->storer = $storer;
        $this->seeker = $seeker;
        $this->context = $context;

        $this->ontologies = $storer->getIndexedOntologies();
    }

    /**
     * Recursively builds a class hierarchy for an array of classes,
     * on the base of the http://www.w3.org/2000/01/rdf-schema#subClassOf predicate.
     *
     * @param array $classes Array of resource's classes (http://www.w3.org/1999/02/22-rdf-syntax-ns#type predicate).
     * @param bool $returnJson
     * @return array|string Hierarchic JSON or array (according to $returnJson param).
     */
    public function getHierarchy(array $classes, bool $returnJson = true)
    {
        $hierarchy = array();
        foreach ($classes as $class)
            $hierarchy = $this->buildHierarchy($hierarchy, $class);

        return $returnJson === true
            ? json_encode($hierarchy,
                          JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES)
            : $hierarchy;
    }

    /**
     * RECURSIVE
     *
     * Based on application ontologies, recursively builds the ascending class hierarchy of a given class.
     * @param array $hierarchy
     * @param string $class
     * @return array
     * @throws \Exception
     */
    private function buildHierarchy(array $hierarchy, string $class): array
    {
        $parentsKeys = ['superClasses', 'directDomains'];
        $classesKey = 'classes';

        foreach ($this->ontologies as $url => $ontology)
        {
            $hierarchy[$class] = $ontology[$classesKey][$class] ?? null;
            if (null === $hierarchy[$class])
                continue;

            $superClasses = (function() use (&$hierarchy, &$class, &$parentsKeys): array {
                $superClasses = array();
                foreach ($parentsKeys as $parentsKey)
                    if (isset($hierarchy[$class][$parentsKey]) && is_array($hierarchy[$class][$parentsKey]))
                        $superClasses = array_merge($superClasses, $hierarchy[$class][$parentsKey]);
                return $superClasses;
            })();

            foreach ($superClasses as $superClass)
                $hierarchy = $this->buildHierarchy($hierarchy, $superClass);
        }

        return $hierarchy;
    }
}
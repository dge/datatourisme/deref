<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Service\Display\Widget;

use App\Service\Ontology\Builder\Entity\RDFGraph;
use Exception;
use Twig_Environment;

/**
 * Manages widgets.
 * @package App\Service\Display\Widget
 */
class WidgetService
{
    /**
     * Template engine.
     * @var Twig_Environment
     */
    private $engine;

    /**
     * Widgets injected to the service.
     * @var array
     */
    private $widgets;

    /**
     * WidgetService constructor.
     * @param Twig_Environment &$engine
     * @param array $widgets
     * @throws Exception
     */
    public function __construct(Twig_Environment $engine, array $widgets)
    {
        $this->engine = $engine;

        $count = count($widgets);
        for ($i=0; $i<$count; $i++)
        {
            if (!$widgets[$i] instanceof AbstractWidget)
                throw new Exception("Widget must be an instance extending AbstractWidget class.");

            $this->widgets[] = $widgets[$i];
        }
    }

    /**
     * Returns widgets injected to the service.
     * @return array
     */
    public function getWidgets(): array
    {
        return $this->widgets;
    }

    /**
     * Get an array of instantiated available widgets.
     * @param RDFGraph $graph
     * @return array
     */
    public function getAvailableWidgets(RDFGraph $graph): array
    {
        $widgets = $this->getWidgets();
        $availableWidgets = array();

        /** @var AbstractWidget $widget */
        foreach ($widgets as $widget)
        {
            $widget->setGraph($graph);
            if ($widget->isAvailable())
            {
                $widget->setTemplateEngine($this->engine);
                $availableWidgets[] = $widget;
            }
        }

        return $availableWidgets;
    }
}
<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Service\Display\Widget;

use App\Service\Ontology\Builder\Entity\RDFGraph;
use Twig_Environment;

/**
 * Interface WidgetInterface
 * @package App\Widget
 */
interface WidgetInterface
{
    /**
     * @return RDFGraph
     */
    public function getGraph(): RDFGraph;

    /**
     * @param RDFGraph $graph
     * @return bool
     */
    public function setGraph(RDFGraph $graph): bool;

    /**
     * @return Twig_Environment
     */
    public function getTemplateEngine(): Twig_Environment;

    /**
     * @param Twig_Environment &$engine
     * @return bool
     */
    public function setTemplateEngine(Twig_Environment $engine): bool;

    /**
     * Removes, from an array of properties, all properties already displayed the widget.
     * @param array $propNames Properties' names to be analyzed and possibly removed. Passed by reference and dynamically modified.
     * @return int Number or properties removed.
     */
    public function removeDisplayedProperties(array $propNames): int;

    /**
     * Returns widget's template.
     * @return string
     */
    public function getTemplate(): string;

    /**
     * Checks if widget is available.
     * @return bool
     */
    public function isAvailable(): bool;

    /**
     * Get widget required data.
     * @return AbstractWidgetData|null
     */
    public function getData();

    /**
     * Sets widget required data.
     * @param AbstractWidgetData|null $data
     * @return mixed
     */
    public function setData(AbstractWidgetData $data = null);
}
<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Controller;

use App\Error\Exception\Query\Sparql\SparqlQueryFailedException;
use App\Error\Exception\Query\Sparql\SparqlQueryMalformedException;
use App\Service\Display\Data\StructuredData;
use App\Service\ERManager\ERManagerService;
use App\Service\Ontology\Builder\Collection\RDFResourceCollection;
use App\Service\Ontology\Builder\Entity\RDFGraph;
use App\Service\Ontology\Context\ContextService;
use App\Sparql\SparqlClient;
use App\Supplier\RDF\RDF;
use EasyRdf\RdfNamespace;
use EasyRdf\Sparql\Client;
use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;

/**
 * Main application controller.
 * Returns a RDF file if a valid format is provided as url parameter, or returns a HTML response if not.
 * @package App\Controller
 */
class IndexController implements ControllerProviderInterface
{
    /**
     * Connect
     * @param Application $app
     * @return mixed
     */
    public function connect(Application $app): ControllerCollection
    {
        $index = $app['controllers_factory'];
        $index
            ->match('/{uri}', array($this, 'index'))
            ->assert('uri', '.*')
            ->bind('index');
        return $index;
    }

    /**
     * Index
     * @param Application $app
     * @param string $uri
     * @param Request $request
     * @return string
     * @throws SparqlQueryFailedException
     */
    public function index(Application $app, string $uri, Request $request): string
    {
        $parameters = $app['config']->getParameters();

        if (isset($parameters['fallback_path'])) {
            if (empty($uri)) {
                header('location: ' . $parameters['fallback_path']);
                die;
            }
        }

        // Preventing injection
        if (strpos($uri, '>') !== false)
            throw new SparqlQueryMalformedException("This query is not allowed due to unsafe design.");

        // Path and parameters
        $path = $parameters['data_path'] . $uri;
        $referencesAllowed = $app['config']->referencesAllowed(
            $parameters['show_references_for'], $path
        );

        // File response?
        if ($format = $request->get('format'))
        {
            $query = $referencesAllowed === true
                ? "CONSTRUCT { <$path> ?p ?o . ?h ?g <$path> .  }
                   WHERE { <$path> ?p ?o .
                       {} UNION { ?h ?g <$path> . }
                   }"
                : "CONSTRUCT WHERE { ?s ?p <$path> }";

            $response = $this->getFileResponse(
                $app['service.er_manager'], $app['ontology.context'],
                $query, $path, $parameters['data_sparql_endpoint'],
                $format, $parameters['data_sparql_fallback_endpoints']
            );
            if ($response)
                return $response;
        }

        // HTML response
        try {
            $sparqlClient = new SparqlClient($parameters['data_sparql_endpoint'], $parameters['data_sparql_fallback_endpoints']);
            $sparqlClient->setHeaders(["Accept: ".$parameters['data_format']]);
            $query      = "
                CONSTRUCT
                {
                    <$path> ?p ?o .
                    ?o <".RDF::MAIN_LABEL_URI."> ?l
                }
                WHERE
                {
                    <$path> ?p ?o
                    {}
                    UNION
                    {
                        ?o <".RDF::MAIN_LABEL_URI."> ?l
                    }
                }
            ";
            $nTriples = $sparqlClient->query($query);
            if (empty($nTriples))
                return $this->get404Response($app, $uri);

            $graph = $app['ontology.builder']->createGraph($nTriples, $path);
        } catch (SparqlQueryFailedException $e) {
            $app['monolog']->critical('SPARQL query error - ' . $e->getMessage());
            if ($parameters['dev_mode'] === true)
                throw $e;

            return $this->get404Response($app, $uri);
        }

        /* @var StructuredData $structuredData */
        $structuredData = $app['service.display']->getStructuredData($graph);
        return $app['twig']->render('app/index.twig', array(
            'uri'          => $structuredData->getUri(),
            'title'        => $structuredData->getTitle(),
            'description'  => $structuredData->getDescription(),
            'types'        => $structuredData->getTypes(),
            'sections'     => $structuredData->getSections(),
            'undescribed'  => $structuredData->getUndescribed(),
            'widgets'      => $structuredData->getWidgets(),
            'allow_refs'   => $referencesAllowed,
            'has_fallbacked' => $sparqlClient->hasFallbacked()
        ));
    }

    /**
     * Returns response as a downloaded file.
     * @param ERManagerService $erManagerService
     * @param ContextService $contextService
     * @param string $query
     * @param string $path
     * @param string $endpoint
     * @param string $format
     * @param array $fallbackEndpoints
     * @return bool|string
     * @throws \Exception
     */
    private function getFileResponse(ERManagerService $erManagerService, ContextService $contextService,
                                     string $query, string $path, string $endpoint, string $format,
                                     array $fallbackEndpoints = array()
    ) {
        $client = new Client($endpoint);
        $result = $client->query($query);
        $graph = $erManagerService->graphFormat($result, $path);
        $erManagerService->addNamespaces($contextService->getPrefixes());

        $response = $erManagerService->format($graph, $format);
        if ($response) {
            return $erManagerService->downloadResponse($response, $path, $format);
        }

        if (!in_array($endpoint, $fallbackEndpoints, TRUE)) {
            foreach ($fallbackEndpoints as $fallbackEndpoint) {
                $response = $this->getFileResponse(
                    $erManagerService, $contextService, $query, $path, $fallbackEndpoint, $format, $fallbackEndpoints
                );
                if ($response) {
                    return $erManagerService->downloadResponse($response, $path, $format);
                }
            }
        }

        return false;
    }

    /**
     * Redirects to 404 template.
     * @param Application $app
     * @param string $uri
     * @return
     */
    private function get404Response(Application $app, string $uri)
    {
        $html = $app['twig']->render('app/not_found.twig', array(
            'uri' => $uri,
            'status_code' => 404
        ));

        return $html;
    }
}
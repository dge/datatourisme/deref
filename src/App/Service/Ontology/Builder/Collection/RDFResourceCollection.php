<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Service\Ontology\Builder\Collection;

use App\Service\Ontology\Builder\Entity\RDFResource;

/**
 * Class RDFResourceCollection
 * @package App\Service\Ontology\Builder\Collection
 */
class RDFResourceCollection extends \ArrayObject
{
    /**
     * RDFResourceCollection constructor.
     * @param RDFResource[] $resources
     */
    public function __construct(array $resources = [])
    {
        foreach ($resources as $resource)
            $this->add($resource);
    }

    /**
     * @param RDFResource $resource
     * @return RDFResourceCollection
     */
    public function add(RDFResource $resource): self
    {
        $this->offsetSet($resource->getUri(), $resource);

        return $this;
    }
}
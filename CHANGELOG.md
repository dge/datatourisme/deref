CHANGELOG
===================

## [v1.3.3](https://gitlab.conjecto.com/datatourisme/deref/tags/v1.3.3) - 2020.12.07

### Changed

- Public template

---------------

## [v1.3.2](https://gitlab.conjecto.com/datatourisme/deref/tags/v1.3.2) - 2019.06.02

### Fixed
- Fix wrong semantic version
- Add OSM credits to map
- Revert submodule update

---------------

## [v1.3.1](https://gitlab.conjecto.com/datatourisme/deref/tags/v1.3.1) - 2019.10.01

### Changed
- Update git submodule

---------------

## [v1.3.0](https://gitlab.conjecto.com/datatourisme/deref/tags/v1.3.0) - 2019.01.14

### Changed
- Update DATAtourisme ontologies URI

----------------

## [v1.2.0](https://gitlab.conjecto.com/datatourisme/deref/tags/v1.2.0) - 2018.05.25

### Changed
- Now displaying red label on obsolete POI 
- Improved properties styling
- Updated WebappBundle | v1.0.3 -> v1.0.8
- File export query not building hierarchy anymore

### Fixed
- Langcodes were not correctly formatted

----------------

## [v1.1.0](https://gitlab.conjecto.com/datatourisme/deref/tags/v1.1.0) - 2018.04.10

### Added
- Fallback endpoints support

### Changed
- Better markup for multiple values
- Label and description now default to french
- Better lang tag formatting
- Updated WebappBundle | v0.1.1-104 -> v1.0.3

### Fixed
- Newlines were not interpreted in some properties
- Location query was randomly crashing

----------------

## [v1.0.2](https://gitlab.conjecto.com/datatourisme/deref/tags/v1.0.2) - 2018.02.14

### Changed
- Updated WebappBundle | v0.1.1-103 -> v0.1.1-104
- Improved changelog layout

### Fixed
- Broken file download on sub-resources 
- Error not properly logged on failed HTML response

----------------

## [v1.0.1](https://gitlab.conjecto.com/datatourisme/deref/tags/v1.0.1) - 2017.12.11

### Fixed
- File export was missing POI itself

---------------- 

## [v0.3.5](https://gitlab.conjecto.com/datatourisme/deref/tags/v0.3.5) / [v1.0](https://gitlab.conjecto.com/datatourisme/deref/tags/v1.0) - 2017.12.11

### Changed
- File export query now getting hierarchy

----------------

## [v0.3.4](https://gitlab.conjecto.com/datatourisme/deref/tags/v0.3.4) - 2017.12.11

### Fixed
- Types were not displayed when no title
- Check if title.label is defined

----------------

## [v0.3.3](https://gitlab.conjecto.com/datatourisme/deref/tags/v0.3.3) - 2017.12.11

## Changed
- City's label in LocationWidget
- POI's label in page's <title>

## Fixed
- Unexpected behaviours in types' spaces

----------------

## [v0.3.2](https://gitlab.conjecto.com/datatourisme/deref/tags/v0.3.2) - 2017.12.08

## Changed
- Using common "not_found" template
- Added busters.json to .gitignore
- Redirecting to fallback URL if no resource URI provided

## Fixed
- OPTIONAL was missing in LocationWidget's query

----------------

## [v0.3.1](https://gitlab.conjecto.com/datatourisme/deref/tags/v0.3.1) - 2017.12.04

### Changed
- Updated WebappBundle | v0.1.1-92 -> v0.1.1-95
- Event Widget above Location Widget

### Fixed
- Regressions brought by Browserify workflow

----------------

## [v0.3](https://gitlab.conjecto.com/datatourisme/deref/tags/v0.3) - 2017.12.01

### Added
- Changelog file
- Event widget

### Changed
- Updated WebappBundle | v0.1.1-89 -> v0.1.1-92
- Renamed "readme.md" to "README.md"

### Fixed
- Empty spaces in list of types
- Adapted Location widget to ontology variations

----------------

## [v0.2.2](https://gitlab.conjecto.com/datatourisme/deref/tags/v0.2.2) - 2017-11-27

### Added
- Can now set label and comment to RDFResource

### Changed
- Labels and comments in "other properties"

### Fixed
- Duplicate properties when no label on section

----------------

## [v0.2.1](https://gitlab.conjecto.com/datatourisme/deref/tags/v0.2.1) - 2017-11-27

### Changed
- Updated WebappBundle | v0.1.1-87 -> v0.1.1-89
- Implemented Browserify workflow
- Suppliers' sources now defined in parameters

### Fixed
- Leaflet was called before DOMContentLoaded
- Could not add multiple paths to same Twig namespace

----------------

## [v0.2](https://gitlab.conjecto.com/datatourisme/deref/tags/v0.2) - 2017-11-24

### Changed
- Larger space between POI's types
- Implemented official header and footer
- Updated WebappBundle | v0.1.1-66 -> v0.1.1-87

### Fixed
- No section was created for a class without label
- Error when trimming quotes on empty strings

<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Controller;

use App\Error\Exception\Query\Sparql\SparqlQueryFailedException;
use App\Error\Exception\Query\Sparql\SparqlQueryMalformedException;
use App\Service\Ontology\Builder\Entity\RDFGraph;
use App\Service\Ontology\Builder\Entity\RDFResource;
use App\Service\Ontology\Context\ContextService;
use App\Service\Ontology\Seeker\SeekerService;
use App\Sparql\SparqlClient;
use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;

/**
 * "References to dereferenced resource". This section is called asynchronously.
 * @package App\Controller
 */
class ReferenceController implements ControllerProviderInterface
{
    /**
     * Connect
     * @param Application $app
     * @return mixed
     */
    public function connect(Application $app): ControllerCollection
    {
        $index = $app['controllers_factory'];
        $index
            ->match('/{uri}', array($this, 'index'))
            ->assert('uri', '.+')
            ->bind('references');
        return $index;
    }

    /**
     * Index
     * @param Request $request
     * @param Application $app
     * @param string $uri
     * @return string
     * @throws SparqlQueryFailedException
     * @throws SparqlQueryMalformedException
     */
    public function index(Request $request, Application $app, string $uri): string
    {
        // Preventing injection
        if (strpos($uri, '>') !== false)
            throw new SparqlQueryMalformedException("This query is not allowed due to unsafe design.");

        // Path and parameters
        /** @var SeekerService $seekerService */
        $seekerService = $app['ontology.seeker'];
        $parameters = $app['config']->getParameters();
        $path = $parameters['data_path'] . $uri;

        // Is the path allowed to query references?
        if (false === $app['config']->referencesAllowed($parameters['show_references_for'], $path))
        {
            return $app['twig']->render('app/not_found.twig', array(
                'uri' => $uri
            ));
        }

        // Querying triplestore and building graph
        $query = "CONSTRUCT WHERE { ?s ?p <$path> }";
        $sparqlClient = new SparqlClient($parameters['data_sparql_endpoint']);
        $sparqlClient->setHeaders(['Accept :'.$parameters['data_format']]);
        $nTriples = $sparqlClient->query($query);
        try {
            /** @var RDFGraph $graph */
            $graph = $app['ontology.builder']->createGraph($nTriples, null);
        } catch (\Exception $e) {
            $app['monolog']->critical('SPARQL query error - ' . $e->getMessage());
            if ($parameters['dev_mode'] === true)
                throw $e;

            return $app['twig']->render('app/not_found.twig', array(
                'uri' => $uri
            ));
        }

        // Flatting triples for HTML response
        $triples = array();
        $k = 0;
        /** @var RDFResource $resource */
        foreach ($graph->getResources() as $resource)
        {
            $triples[$k]['s']['label'] = null;
            $triples[$k]['s']['uri'] = $resource->getUri();

            $pUri = $resource->getFirstResource()->getUri();
            $triples[$k]['p']['label'] = $seekerService->findClassLabel($pUri);
            $triples[$k]['p']['uri'] = $pUri;

            $triples[$k]['o']['label'] = $request->query->get('res_label');
            $triples[$k]['o']['uri'] = $resource->getFirstResource()->getFirstLeaf();

            ++$k;
        }
        return $app['twig']->render('app/references.twig', array(
            'triples'   => $triples,
        ));
    }
}
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var gulp = require('gulp');
var browserify = require('browserify');
var watchify = require('watchify');
var source = require('vinyl-source-stream');
var livereload = require('gulp-livereload');
var glob = require('glob');
var es = require('event-stream');
var path = require('path');
var uglify = require('gulp-uglify-es').default;

var $ = require('gulp-load-plugins')({
    pattern: ['gulp-*']
});

/**
 * Variables
 */
var entries = glob.sync('./app/Resources/scripts/*.js');
var dstDir = './web/assets/scripts';

/**
 * @param obfuscate
 * @param watch
 * @param dest
 */
function run(obfuscate, watch, dest) {
    var stream = es.merge(entries.map(function(entry) {
        return build(entry, obfuscate, watch, dest);
    }));
    return stream;
}

/**
 * @param input
 * @param obfuscate
 * @param watch
 * @param dest
 * @returns {*}
 */
function build(input, obfuscate, watch, dest) {
    var bundler = browserify({
        entries: input,
        debug: false,
        cache: {},
        packageCache: {},
        extensions: ['.js', '.json', '.jsx'],
        //basedir: srcDir,
        //paths: ["."],
        fullPaths: false
    });

    //bundler = bundler.transform('require-globify', {global: true});
    //bundler = bundler.transform('stringify', {
    //    global: true,
    //    minify: true
    //});

    // babelify ?
    //bundler = bundler.transform('babelify', {presets: ['es2015', 'react']});

    function bundle(b) {
        var startMs = Date.now();
        var db = b.bundle()
            .on('error', function(error) {
                $.util.log($.util.colors.red(error.message));
                $.util.log($.util.colors.red(error.stack));
            })
            .pipe(source(path.basename(input)));

        if (obfuscate) {
            db/*.pipe($.streamify($.ngAnnotate()))*/
                .pipe($.streamify(uglify()))
                .on('error', function(error) {
                    $.util.log($.util.colors.red(error.toString()));
                });
        }

        // optional, remove if you dont want sourcemaps
        //db.pipe(sourcemaps.init({loadMaps: true}));       // loads map from browserify file
        //db.pipe(sourcemaps.write('./'));                  // Add transformation tasks to the pipeline here.

        db.pipe(gulp.dest(dest));
        console.log('update js -', (Date.now() - startMs) + 'ms');
        return db;
    }

    if (watch) {
        bundler = watchify(bundler)
            .on('update', function() {
                bundle(bundler).pipe(livereload());
            });
    }

    return bundle(bundler);
}

/**
 * @type {{build: module.exports.build, watch: module.exports.watch}}
 */
module.exports = {
    build: function() {
        return run(true, false, dstDir);
    },
    watch: function() {
        livereload.listen();
        run(false, true, dstDir);
    }
};

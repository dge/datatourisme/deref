<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Service\Display\Data;

use App\Service\Ontology\Builder\Entity\RDFGraph;

class StructuredData
{
    /**
     * @var array
     */
    private $uri;

    /**
     * @var array
     */
    private $title;

    /**
     * @var string
     */
    private $description;

    /**
     * @var array
     */
    private $types;

    /**
     * @var array
     */
    private $sections;

    /**
     * @var array
     */
    private $undescribed;

    /**
     * @var array
     */
    private $widgets;

    /**
     * @var RDFGraph
     */
    private $graph;

    /**
     * StructuredData constructor.
     * @param RDFGraph $graph
     */
    public function __construct(RDFGraph $graph)
    {
        $this->graph = $graph;

        $this->uri = array();
        $this->title = array();
        $this->types = array();
        $this->sections = array();
        $this->undescribed = array();
        $this->widgets = array();
    }

    /**
     * @return array|null
     */
    public function getUri(): array
    {
        return $this->uri;
    }

    /**
     * @param array $uri
     * @return self
     */
    public function setUri(array $uri): self
    {
        $this->uri = $uri;
        return $this;
    }

    /**
     * @return array
     */
    public function getTitle(): array
    {
        return $this->title;
    }

    /**
     * @param array $title
     * @return self
     */
    public function setTitle(array $title): self
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return self
     */
    public function setDescription(string $description = null): self
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return array
     */
    public function getTypes(): array
    {
        return $this->types;
    }

    /**
     * @param array $types
     * @return self
     */
    public function setTypes(array $types): self
    {
        $this->types = $types;
        return $this;
    }

    /**
     * @return array
     */
    public function getSections(): array
    {
        return $this->sections;
    }

    /**
     * @param array $sections
     * @return self
     */
    public function setSections(array $sections): self
    {
        $this->sections = $sections;
        return $this;
    }

    /**
     * @return array
     */
    public function getUndescribed(): array
    {
        return $this->undescribed;
    }

    /**
     * @param array $undescribed
     * @return self
     */
    public function setUndescribed(array $undescribed): self
    {
        $this->undescribed = $undescribed;
        return $this;
    }

    /**
     * @return array
     */
    public function getWidgets(): array
    {
        return $this->widgets;
    }

    /**
     * @param mixed $widgets
     * @return self
     */
    public function setWidgets(array $widgets): self
    {
        $this->widgets = $widgets;
        return $this;
    }

    /**
     * @return RDFGraph
     */
    public function getGraph(): RDFGraph
    {
        return $this->graph;
    }

    /**
     * @param RDFGraph $graph
     * @return self
     */
    public function setGraph(RDFGraph $graph): self
    {
        $this->graph = $graph;
        return $this;
    }
}
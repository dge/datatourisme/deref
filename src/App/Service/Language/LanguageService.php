<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Service\Language;
use App\Error\Exception\Format\Standard\InvalidJsonException;
use App\Service\Language\Storer\LangSpecificationStorer;
use App\Service\Language\Supplier\Rfc1766\Rfc1766LanguageSupplier;

/**
 * Class LanguageService
 * @package App\Service\Ontology\LanguageDefinitions
 */
class LanguageService
{
    /**
     * @var int
     */
    const RFC_1766 = 'rfc_1766';

    /**
     * @var array
     */
    private $list;

    /**
     * @var LangSpecificationStorer
     */
    private $specificationStorer;

    /**
     * LanguageDefinitions constructor.
     * @param array $suppliers
     * @param array $parameters
     */
    public function __construct(array $suppliers = array(), array $parameters = array())
    {
        $defaultSuppliers = array(
            new Rfc1766LanguageSupplier(self::RFC_1766, true)
        );

        $suppliers = array_merge($defaultSuppliers, $suppliers);

        $this->specificationStorer = new LangSpecificationStorer($suppliers, $parameters);
    }

    /**
     * Gets string language from code, based on specified language and code specification.
     *
     * Usage example: ("it", "fr", "rfc_1766") would return "Italien".
     *
     * @param string $code
     * @param string $lang
     * @param string $specification
     * @return string|null
     */
    public function getLangFromCode(string $code, string $lang = 'fr', string $specification = self::RFC_1766)
    {
        $list = $this->getSpecificationList($specification);

        return $list[$code][$lang] ?? null;
    }

    /**
     * Gets list of languages.
     * @param string $specification
     * @return array
     * @throws \Exception
     */
    public function getSpecificationList(string $specification = self::RFC_1766): array
    {
        $specification = strtolower($specification);
        if ($this->list[$specification])
            return $this->list[$specification];

        $list = $this->specificationStorer->getSpecification($specification);

        if (!$list)
            throw new \Exception('Couldn\'t find languages list for given "'.strtoupper($specification).'" specification.');

        $this->list[$specification] = json_decode($list, true);

        if ($this->list[$specification] === null)
            throw new InvalidJsonException('JSON error while parsing "'.strtoupper($specification).'" language specification.'
                . (json_last_error() !== JSON_ERROR_NONE ? ' ---- Message: '.json_last_error_msg() : '')
            );

        return $this->list[$specification];
    }
}
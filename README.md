# DATAtourisme - Dereferencing interface

## 1 - Summary
According to given route, dereferences RDF resource from dataset and uses provided ontologies to display (HTML) or download it (RDF/XML, N-Triples, Turtle, JSON-LD).

## 2 - Installation steps

### 2.1 - Dependencies:
```bash
# /
git submodule init && git submodule update
composer install
npm install
```

### 2.2 - Assets:
```bash
# /
gulp build:dist
```

### 2.3 - Configuration:
```yaml
# /app/config/parameters.yml
parameters:
  data_sparql_endpoint: {url-to-endpoint}
  data_path: {base-path-for-uris}
  data_format: {accept-format-from-endpoint}
```

### 2.4 - Suppliers
Some features of the application rely on what we'll call suppliers.

* In the namespace ```App\Supplier\Ontology```,
    we must create at least one Ontology provider class, which will extend
    ```App\Service\Ontology\AbstractOntology``` and implement a public ```provideOntology()``` method.
    The role of this method is to return an RDF Ontology from any source.
    The output format must be a valid JSON string.
* In the namespace ```App\Supplier\Widget```, we provide widgets for the application.
    Each Widget class must extend ```App\Service\Display\Widget\AbstractWidget``` and has to implement two public methods:
    ```isAvailable()``` determines if the widget will display in the current page set, and ```getData()``` provides
    the data for the widget template from any source. The output data must be an instance extending
    ```App\Service\Display\Widget\AbstractWidgetData```. An HTML template has to be created for each widget.
    
**NB:** While installing a project with application specific ontologies and widgets already configured,
    you might have to customize potential distant sources in ```provideOntology()```
    and ```getData()``` methods from respective classes.
    

### 2.5 - Usage:
"*/web/index.php*" bootstrap file must be served by a web server.

Rapid local test example with built-in PHP web server:
```bash
# /web/
php -S 127.0.0.1:8000
```
In that case, URI will look like "*http://localhost:8000/it38-PlaceOfInterest-456*".


## 3 - Documentation
```bash
# /doc/
sh generate.sh
```
These command lines will use phpDocumentor to generate a "*/doc/doc/*" folder containing "*index.html*" file.

## 4 - Tests

### 4.1 - All suites
To run all tests at once:
```bash
# /
phpunit
```

### 4.2 - Testsuites
See "*/phpunit.xml.dist*" for precise testsuites. Example:
```bash
# /
phpunit --testsuite Suppliers
```
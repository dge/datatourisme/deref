<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Sparql;

/**
 * Class SparqlBuilder
 *
 * Experimental. Should not be used.
 * @package App\Sparql
 */
class SparqlBuilder
{
    /**
     * @var string
     */
    private $sparql;

    /**
     * SparqlBuilder constructor.
     */
    public function __construct()
    {
        $this->sparql = "";
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->sparql;
    }

    /**
     * @param string $prefix
     * @param string $uri
     * @return SparqlBuilder
     * @throws \UnexpectedValueException
     */
    public function addPrefix(string $prefix, string $uri): self
    {
        $trimmedUri = trim($uri, "<>");
        if (filter_var($trimmedUri, FILTER_VALIDATE_URL) === false)
            throw new \UnexpectedValueException("Must provide valid URI.");

        $str = "prefix $prefix: <$trimmedUri>";
        $this->addStr($str);

        return $this;
    }

    /**
     * @param array $prefixes
     * @return SparqlBuilder
     * @throws \UnexpectedValueException
     */
    public function addPrefixes(array $prefixes): self
    {
        if (empty($prefixes))
            throw new \UnexpectedValueException("Must provide at least one element.");

        foreach ($prefixes as $prefix => $uri)
            $this->addPrefix($prefix, $uri);

        return $this;
    }

    /**
     * Returns SPARQL Query as String
     * @return string
     */
    public function getSparql(): string
    {
        return $this->sparql;
    }

    /**
     * @param string $str
     * @return SparqlBuilder
     */
    private function addStr(string $str): self
    {
        $this->sparql = $this->sparql . " $str";

        return $this;
    }
}
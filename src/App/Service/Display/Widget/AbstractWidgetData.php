<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Service\Display\Widget;

/**
 * Class AbstractWidgetData
 * @package App\Service\Display\Widget
 */
class AbstractWidgetData
{
    /**
     * @var string
     */
    private $widgetClass;

    /**
     * AbstractWidgetData constructor.
     * @param string $widgetClass
     */
    public function __construct(string $widgetClass)
    {
        $this->setWidgetClass($widgetClass);
    }

    /**
     * @return string
     */
    public function getWidgetClass(): string
    {
        return $this->widgetClass;
    }

    /**
     * @param string $widgetClass
     */
    public function setWidgetClass(string $widgetClass)
    {
        if (!class_exists($widgetClass))
            throw new \UnexpectedValueException("Class \"$widgetClass\" does not exist.");

        $this->widgetClass = $widgetClass;
    }
}
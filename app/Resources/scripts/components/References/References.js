/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
"use strict";

const List = require('list.js');
const $ = require('jquery');

module.exports = class References
{
    /**
     * References constructor.
     */
    constructor()
    {
        this.listOptions = {
            listClass: "list",
            indexAsync: true,
            valueNames: ['s', 'p', 'o'],
            page: 20,
            pagination: {
                paginationClass: "paginationClass",
                innerWindow: 2,
                outerWindow: 2,
            },
        };
    }

    /**
     * Boots References.
     * @param container
     */
    run(container)
    {
        if (!container instanceof HTMLElement)
            throw "Container must be an instance of HTMLElement";

        this.request(container);
    }

    /**
     * Executes AJAX request.
     * @param container
     * @returns {*}
     */
    request(container)
    {
        container.innerHTML = this.getLoadingTemplate();

        const path = container.getAttribute('data-path');
        let xhr = new XMLHttpRequest();
        xhr.open('GET', path, true);
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    container.innerHTML = xhr.responseText;
                    const boxPagination = document.getElementById("box-pagination");
                    const pageSize = Number(boxPagination.getAttribute("data-ref-pagesize"));
                    const triplesN = Number(boxPagination.getAttribute("data-ref-triples-n"));
                    if (triplesN > pageSize) {
                        this.paginate({
                            page: pageSize
                        });
                    } else {
                        boxPagination.style.display = "none";
                    }
                } else {
                    container.innerHTML = `Erreur - La requête n'a pas abouti.`;
                }

            }
        };

        xhr.send();

        return container;
    }

    /**
     * Initializes pagination.
     * @returns {boolean}
     */
    paginate(options = {})
    {
        const mergedOptions = Object.assign(this.listOptions, options);

        const list = new List("listContainer", mergedOptions);

        if (!list)
            throw "Error occurred while paginating references.";

        this.initNav('btn-nav', 'btn-prev', 'btn-next');

        return true;
    }

    /**
     * Initializes "Previous" and "Next" buttons.
     * @returns {boolean}
     */
    initNav(btnClass, prevId, nextId)
    {
        let id;
        const paginationClass = this.listOptions.pagination[0].paginationClass;
        const pages = '.' + paginationClass + ' > li';

        let process = (element = null) => {
            $(pages).addClass(btnClass);

            if (element)
                id = $(element).attr('id');

            $(pages).each(function(index, element) {
                if($(element).is('.active')) {
                    if (id === prevId || id === nextId) {
                        if (id === prevId)
                            index--;
                        else if (id === nextId)
                            index++;

                        $($(pages)[index]).trigger('click');
                    }

                    if (index <= 0)
                        $('#' + prevId).addClass('disabled');
                    else
                        $('#' + prevId).removeClass('disabled');

                    if (index >= $(pages).length - 1)
                        $('#' + nextId).addClass('disabled');
                    else
                        $('#' + nextId).removeClass('disabled');
                }
            });
        };

        process(null);
        $('.' + btnClass).on("click", function() { process(this); });
        $('.box-footer').on("click", function() { process(this); });

        return true;
    }

    /**
     * Gets Loading Template
     * @returns {string}
     */
    getLoadingTemplate()
    {
        return `
            &nbsp;Recherche...&nbsp;
            <img alt=\"Loading...\" src=\"/assets/images/spinner.gif\" />
        `;
    }
}
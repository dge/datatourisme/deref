<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Service\Ontology;


/**
 * Provides ontology from distant source or from cache if enabled.
 * Each provider's goal is to furnish ontology from its own sources.
 * @package App\Supplier\Ontology
 */
interface OntologyInterface
{
    /**
     * ProviderInterface constructor.
     * @param bool $cache If true, provider will use cache system. If not, it will load ontology from distant source each time getOntology() is called.
     * @param string $url
     * @param string $namespace
     */
    public function __construct(bool $cache, string $url, string $namespace);

    /**
     * Returns ontology base url.
     * @return string
     */
    public function getUrl(): string;

    /**
     * Returns ontology shorten namespace.
     * @return string
     */
    public function getNamespace(): string;

    /**
     * Returns ontology.
     * @return string
     */
    public function getOntology(): string;

    /**
     * Get application parameters.
     * @return array
     */
    public function getParameters(): array;

    /**
     * Set application parameters.
     * @param array $parameters
     * @return bool
     */
    public function setParameters(array $parameters): bool;

    /**
     * @return mixed
     */
    public function getCache();

    /**
     * @param string $folder
     * @return mixed
     */
    public function setCache(string $folder);
}
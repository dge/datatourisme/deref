<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Supplier\Ontology;
use App\Service\Ontology\AbstractOntology;
use Symfony\Component\Yaml\Yaml;

/**
 * Class Datatourisme
 * @package App\Supplier\Ontology
 */
class Datatourisme extends AbstractOntology
{
    /**
     * {@inheritdoc}
     */
    public function __construct(bool $cache, string $url = 'https://www.datatourisme.gouv.fr/ontology/core', string $namespace = "datatourisme")
    {
        parent::__construct($cache, $url, $namespace);
    }

    /**
     * {@inheritdoc}
     */
    public function provideOntology(): string
    {
        $config = Yaml::parse(file_get_contents('../app/config/parameters.yml'));
        $sourcePath = $config['parameters']['suppliers']['ontologies']['datatourisme']['source'] ?? null;

        if (!$sourcePath)
            throw new \UnexpectedValueException("Could not fetch ontology from distant source: path missing.");

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $sourcePath,
            CURLOPT_RETURNTRANSFER => true,
        ));
        $ontology = curl_exec($ch);

        if (!empty($e = curl_error($ch)))
            throw new \Exception($e);

        curl_close($ch);

        $ontology = json_decode($ontology, true);
        $ontology = json_encode($ontology, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

        return $ontology;
    }
}
<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Service\Language\Supplier;

use Symfony\Component\Cache\Adapter\FilesystemAdapter;

/**
 * Class AbstractLangSpecification
 * @package App\Service\Language\Supplier
 */
abstract class AbstractLangSpecification implements LangSpecificationInterface
{
    /**
     * @var string
     */
    protected $namespace;

    /**
     * @var bool
     */
    protected $cacheEnabled;

    /**
     * @var FilesystemAdapter
     */
    protected $cache;

    /**
     * {@inheritdoc}
     */
    public function __construct(string $namespace, bool $cacheEnabled = true)
    {
        $this->namespace = $namespace;
        $this->cacheEnabled = $cacheEnabled;
    }

    /**
     * {@inheritdoc}
     */
    public function getNamespace(): string
    {
        return $this->namespace;
    }

    /**
     * {@inheritdoc}
     */
    public function setNamespace(string $namespace): LangSpecificationInterface
    {
        $this->namespace = $namespace;
        return $this;
    }

    public function getCacheEnabled(): bool
    {
        return $this->cacheEnabled;
    }

    public function setCacheEnabled(bool $cacheEnabled): LangSpecificationInterface
    {
        $this->cacheEnabled = $cacheEnabled;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setCache(string $folder): LangSpecificationInterface
    {
        $this->cache = new FilesystemAdapter($this->namespace, 0, $folder);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCache()
    {
        return $this->cache;
    }

    /**
     * {@inheritdoc}
     */
    public function getSpecification(): string
    {
        return $this->cacheEnabled
            ? $this->cacheSpecification()
            : $this->provideSpecification();
    }

    /**
     * User defined when a new language specification provider class has to be created and injected to the language specification service.
     * @return string
     */
    abstract protected function provideSpecification(): string;

    /**
     * If language specification is already cached, directly returns it from cache. If not, first caches it from provider source then returns it.
     * @return string
     */
    private function cacheSpecification(): string
    {
        $specification = $this->cache->getItem($this->getNamespace());
        if ($specification->isHit() === false)
        {
            $specification->set($this->provideSpecification());
            $this->cache->save($specification);
        }

        return $specification->get();
    }
}
<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Service\Config;

use App\Error\Exception\Configuration\MissingConfigurationException;
use Exception;
use Symfony\Component\Yaml\Yaml;

/**
 * Manages application configurations.
 * @package App\Service\Config
 */
class ConfigService
{
    /**
     * Application parameters.
     * @var array
     */
    private $parameters = array();

    /**
     * ConfigService constructor.
     * @param string $path Path to Yaml config file.
     * @throws \Exception
     */
    public function __construct(string $path = "../app/config/parameters.yml")
    {
        if (!file_exists($path))
            throw new \Exception("Error - File \"$path\" must be created from default model.");

        $this->parameters = Yaml::parse(file_get_contents($path));
        $this->parameters = $this->parameters['parameters'];
    }

    /**
     * Get application configuration parameters.
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * Get specific application configuration parameter.
     * @param string $key
     * @return string|array
     * @throws \Exception
     */
    public function getParameter(string $key)
    {
        if (!isset($this->parameters[$key]))
            throw new MissingConfigurationException("Error - Parameter \"$key\" does not exist.");

        return $this->parameters[$key];
    }

    /**
     * Compares a list of base paths to given path.
     *
     * Examples:
     * `referencesAllowed([http://foo.org/, http://bar.com/], "http://bar.com/hello/")` would return true
     * `referencesAllowed([http://foo.org/], "http://bar.com/hello/")` would return false
     *
     * @param array $allowedPaths List of allowed base paths
     * @param string $path Given path to be compared
     * @return bool
     */
    public function referencesAllowed(array $allowedPaths, string $path): bool
    {
        foreach ($allowedPaths as $allowedPath)
            if (strpos($path, $allowedPath) === 0)
                return true;
        return false;
    }
}